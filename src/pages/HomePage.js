'use strict';
const selectors = {
    'composeButton': '[gh=\'cm\']',
    'searchInputText': 'input[name=\'q\']',
    'sentMailLink': 'a[title=\'Sent Mail\']',
};

class HomePage {

    constructor() {
        this.composeButton = $(selectors.composeButton);
        this.searchInputText = $(selectors.searchInputText);
        this.sentMailLink = $(selectors.sentMailLink);
    }

    isLoaded() {
        return PID.waitForPageToBeLoaded().then(() => {
            return PID.waitForElement(this.searchInputText);
        })
    };

    clickComposeButton() {
        return this.composeButton.click();
    }

    clickSentMailLink() {
        return this.sentMailLink.click();
    }

};

module.exports = new HomePage();