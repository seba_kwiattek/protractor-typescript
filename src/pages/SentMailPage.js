'use strict';
const selectors = {
    'emailTitles': 'span[class=\'bog\']',
    'messageBody': 'div[dir=\'ltr\']',

};

class SentMailPage {

    constructor() {
        this.emailTitles = $(selectors.emailTitles);
        this.messageBody = $(selectors.messageBody);
    }

    isLoaded() {
        return PID.waitForPageToBeLoaded().then(() => {
            return PID.sleep(2000);
        });
    }

    isEmailDisplayed(emailTitle) {
        return element.all(by.css(selectors.emailTitles)).getText().then((arr) => {
            return arr.includes(emailTitle)
        });
    };
        // return element.all(by.css(selectors.emailTitles)).getText().then((arr) => {
        //     var elemIndex = arr.indexOf(emailTitle);
        //     if (elemIndex !== -1) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // });
    // };

};

module.exports = new SentMailPage();