'use strict';
const selectors = {
    'userInputText': '#identifierId',
    'nextButton': '#identifierNext',
    'passwordInputText': 'input[type="password"]',
    'secondNextButton': '#passwordNext',
    'invalidLoginErrorMessage': '#password > div:nth-child(2) > div:nth-child(2)',
};

class LoginPage {

    constructor() {
        this.userInputText = $(selectors.userInputText);
        this.nextButton = $(selectors.nextButton);
        this.passwordInputText = $(selectors.passwordInputText);
        this.secondNextButton = $(selectors.secondNextButton);
        this.invalidLoginErrorMessage = $(selectors.invalidLoginErrorMessage);
    }

    navigateTo() {
        return PID.open(browser.params.env.hostname).then(() => {
            return PID.waitForElement(this.userInputText)
        })
    };

    setUserInputText(value) {
        return this.userInputText.sendKeys(value);
    };

    setPasswordInputText(value) {
        return PID.waitForElement(this.passwordInputText).then(() => {
            return this.passwordInputText.sendKeys(value);
        })
    };

    clickNextButton() {
        return this.nextButton.click();
    };

    clickSecondNextButton() {
        return this.secondNextButton.click();
    };

    login(userName, password) {
        return this.setUserInputText(userName).then(() => {
            return this.clickNextButton()
        }).then(() => {
            return this.setPasswordInputText(password);
        }).then(() => {
            return this.clickSecondNextButton();
        });
    };

    getWrongPasswordErrorMessage() {
        return PID.waitForElement(this.invalidLoginErrorMessage).then(() => {
            return this.invalidLoginErrorMessage.getText();
        })
    };

};

module.exports = new LoginPage();