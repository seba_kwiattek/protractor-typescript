'use strict';

//const protractor = require('protractor');

class PID {

    static open(url) {
        return browser.get(url).then(() => {
            return this.waitForPageToBeLoaded()
        });
    };

    static executeScript(script) {
        return browser.executeScript(script);
    };

    static waitForPageToBeLoaded() {
        return browser.wait(() => {
            return this.executeScript('return document.readyState==="complete"').then((text) => {
                return text === true;
            });
        }, 5000)
    };

    static waitForElement(element) {
        return browser.wait(EC.and(EC.presenceOf(element), EC.visibilityOf(element)), 15000).then(() => {
            return true;
        }).catch((e) => {
            console.log(e);
            return false;
        });
    };

    static sleep(time) {
        return browser.driver.sleep(time);
    };

};

module.exports = PID;