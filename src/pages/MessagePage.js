'use strict';
const selectors = {
    'sendButton': '[tabindex=\'1\'][role=\'button\']:nth-child(2):nth-last-child(2)',
    'toInputText': 'textarea[name="to"]',
    'subjectInputText': 'input[name="subjectbox"]',
    'attachmentButton': '[tabindex=\'1\'][role=\'button\']:nth-child(1):nth-last-child(5)',
    'messageBodyInputText': 'div[aria-label=\'Message Body\']',
    'fileInputText': 'input[multiple]'
};

class MessagePage {

    constructor() {
        this.sendButton = $(selectors.sendButton);
        this.toInputText = $(selectors.toInputText);
        this.subjectInputText = $(selectors.subjectInputText);
        this.attachmentButton = $(selectors.attachmentButton);
        this.messageBodyInputText = $(selectors.messageBodyInputText);
        this.fileInputText = $(selectors.fileInputText)
        //this.sendButton = element(by.cssContainingText('.T-I-Zf-aw2'));
    }

    isLoaded() {
        return PID.waitForPageToBeLoaded().then(() => {
            return PID.waitForElement(this.sendButton);
        })
    };

    clickSendButton() {
        return this.sendButton.click();
    };

    setToInputText(emailAddress) {
        return this.toInputText.sendKeys(emailAddress);
    };

    setSubjectInputText(emailSubject) {
        return this.subjectInputText.sendKeys(emailSubject);
    };

    attacheFile(file) {
        const path = require('path');
        let fileToUpload = '../features/data/' + file;
        let absolutePath = path.resolve(__dirname, fileToUpload);

        return this.attachmentButton.click().then(() => {
        }).then(() => {
            return PID.sleep(1000);
        }).then(() => {
            return browser.executeScript("arguments[0].style.visibility = 'visible'; arguments[0].style.height = '300px'; arguments[0].style.width = '300px';  arguments[0].style.opacity = 1; arguments[0].style.top = '300px';", this.fileInputText.getWebElement());
        }).then(() => {
            return this.fileInputText.sendKeys(absolutePath);
        }).then(() => {
            return PID.sleep(3000);
        }).then(() => {
            return browser.actions().sendKeys(protractor.Key.ENTER).perform();
        }).then(() => {
            return PID.sleep(2000);
        });

    };

    sendMessageWithTimestamp(timestamp) {
        return this.subjectInputText.sendKeys(timestamp).then(() => {
            return this.clickSendButton().then(() => {
            });
        });
    };

    setMessageBodyInputText(message) {
        return this.messageBodyInputText.sendKeys(message);
    }

};

module.exports = new MessagePage();