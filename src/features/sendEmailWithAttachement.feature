Feature: Send Email

  Background:
    Given I navigate to Login page
    And I enter "sii.message.sender" and "messageSender" on Login page
    Then I should see "Home" page

  Scenario Outline: Send_Email_With_Attachment
    Given I click "COMPOSE" button on "Home" page
    And I should see "New Message" window
    When I enter "<emailAddress>" into "To" field on "Message" window
    And I attache file "<pathToFile>" to "Message" window
    Then "Message" with timestamp should be in "Send category"

    Examples:
      | emailAddress                 | pathToFile |
      | sii.message.sender@gmail.com | sample.txt |