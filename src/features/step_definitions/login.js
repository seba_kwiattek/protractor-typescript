'use strict';
const loginPage = require('../../pages/LoginPage.js');
const homePage = require('../../pages/HomePage.js');

const steps = function () {

    this.Given(/^I navigate to Login page$/, function () {
        return loginPage.navigateTo()
    });

    this.When(/^I enter "(.*)" and "(.*)" on Login page$/, function (userName, password) {
        return loginPage.login(userName, password)
    });

    this.Then(/^I should see "(.*)" message on Login page$/, function (message) {
        return expect(loginPage.getWrongPasswordErrorMessage()).to.eventually.equal(message);
    });

};

module.exports = steps;