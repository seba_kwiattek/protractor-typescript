'use strict';
const loginPage = require('../../pages/LoginPage.js');
const homePage = require('../../pages/HomePage.js');

const steps = function () {

    this.Then(/^I should see "Home" page$/, function () {
        return expect(homePage.isLoaded()).to.eventually.be.true;
    });

    this.Given(/^I click "COMPOSE" button on "Home" page$/, function () {
        return homePage.clickComposeButton();
    });
};

module.exports = steps;