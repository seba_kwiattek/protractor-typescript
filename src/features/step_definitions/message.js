'use strict';
const messagePage = require('../../pages/MessagePage.js');
const homePage = require('../../pages/HomePage.js');
const sentMailPage = require('../../pages/SentMailPage.js');

const steps = function () {

    this.When(/^I click "Send" button on "Message" window/, function () {
        return messagePage.clickSendButton();
    });

    this.When(/^I enter "(.*)" into "To" field on "Message" window$/, function (emailAddress) {
        return messagePage.setToInputText(emailAddress);
    });

    this.When(/^I should see "New Message" window$/, function () {
        return messagePage.isLoaded();
    });

    this.When(/^I enter "(.*)" into "Subject" field on "Message" window$/, function (emailSubject) {
        return messagePage.setSubjectInputText(emailSubject);
    });

    this.When(/^I attache file "(.*)" to "Message" window$/, function (filePath) {
        return messagePage.attacheFile(filePath);
    });

    this.When(/^"Message" with timestamp should be in "Send category"$/, function () {
        var timestamp = Date.now().toString();

        return messagePage.sendMessageWithTimestamp(timestamp).then(() => {
            return homePage.clickSentMailLink().then(() => {
                return sentMailPage.isLoaded().then(() => {
                    return expect(sentMailPage.isEmailDisplayed(timestamp)).eventually.to.be.true;
                });
            });
        });
    });

    this.When(/^I enter "(.*)" into "Message Body" field on "Message" window$/, function (message) {
        return messagePage.setMessageBodyInputText(message);
    });
};

module.exports = steps;