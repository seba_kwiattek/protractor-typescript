Feature: Login to Gmail

  Scenario Outline: Login_To_Gmail_Negative
    Given I navigate to Login page
    When I enter "<username>" and "<password>" on Login page
    Then I should see "Wrong password. Try again or click Forgot password to reset it." message on Login page

    Examples:
      | username           | password      |
      | sii.message.sender | badPassword   |
      | badUserName        | messageSender |


  Scenario Outline: Login_To_Gmail_Positive
    Given I navigate to Login page
    When I enter "<username>" and "<password>" on Login page
    Then I should see "Home" page

    Examples:
      | username           | password      |
      | sii.message.sender | messageSender |