Feature: Send Email

  Background:
    Given I navigate to Login page
    And I enter "sii.message.sender" and "messageSender" on Login page
    Then I should see "Home" page

  Scenario Outline: Send_Email_Empty_Message
    Given I click "COMPOSE" button on "Home" page
    And I should see "New Message" window
    When I enter "<emailAddress>" into "To" field on "Message" window
    And I enter "<message>" into "Message Body" field on "Message" window
    Then "Message" with timestamp should be in "Send category"

    Examples:
      | emailAddress                 | message |
      | sii.message.sender@gmail.com | aaabbb  |