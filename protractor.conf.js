'use strict';
var chai = require('chai');
var PID = require('./src/pages/PIDriver');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

exports.config = {
    directConnect: false,
    seleniumAddress: 'http://localhost:4444/wd/hub',

    specs: [
        './src/features/login.feature',
        './src/features/sendEmailEmptyMessage.feature',
        './src/features/sendEmailWithAttachement.feature',
        './src/features/sendEmailWithMessage.feature',
    ],

    capabilities: {
        'browserName': 'chrome',
         'shardTestFiles': true,
         'maxInstances': 1
    },

    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    cucumberOpts: {
        monochrome: true,
        strict: true,
        require: [
            './env.js',
            './src/features/step_definitions/*.js'
        ]
    },

    params: {
        env: {
            hostname: 'http://gmail.com'
        }
    },

    onPrepare: function () {
        browser.ignoreSynchronization = true;
        chai.config.truncateThreshold = 0;
        global.expect = chai.expect;
        global.chai = chai;
        global.PID = PID;
        global.EC = protractor.ExpectedConditions;
    }
};
